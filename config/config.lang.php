<?php
/*
   ------------------------------------------------------------
   语言配置信息
   功能说明: 统一配置系统相关语言
   作者: pinyht
   ------------------------------------------------------------
 */

define('FETCH_FAILD','抓取失败');
define('FETCH_SUCCES','抓取成功');
define('GET_FAILD','获取失败');
define('FETCH_TIMEOUT','抓取超时');
define('TITLE','标题');
define('WUBATONGCHENG','58同城');
define('WUBATONGCHENG_KEY','wubatongcheng');
define('SOUFUNWANG','搜房网');
define('GANJIWANG','赶集网');
define('GUIZHSHENG','贵州省');
define('GUIYANGSHI','贵阳市');
define('TONGRENSHI','铜仁市');
define('ZUNYISHI','遵义市');
define('ANSHUNSHI','安顺市');
define('LIUPANSHUISHI','六盘水市');
define('BIJIESHI','毕节市');
define('LIEBIAOYEMIAN','列表页面');
define('XINXILIEBIAO','信息列表');
define('XIANGXIYEMIAN','详细页面');
define('LIANXIRENMINGZI','联系人名字');
define('LIANXIRENLEIBIE','联系人类别');
define('LIANXIRENDIANHUA','联系人电话');
define('ZUFANGXINXIBIAOTI','租房信息标题');
define('ZUJIN','租金');
define('HUXING','户型');
define('MIANJI','面积');
define('WUYELEIXING','物业类型');
define('ZHUANGXIU','装修');
define('CHAOXIANG','朝向');
define('LOUCENG','楼层');
define('PEITAO','配套');
define('QUYU','区域');
define('XIAOQU','小区');
define('DIZHI','地址');
define('RUZHUSHIJIAN','入住时间');
define('JIESHAO','介绍');
define('TUPIAN','图片');

?>
