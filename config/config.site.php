<?php
/*
   ------------------------------------------------------------
   需要抓取的网站配置信息
   功能说明: 需要抓取的网站的相关配置
   作者: pinyht
   ------------------------------------------------------------
 */

// 采用数组的形式来存放城市名
$siteConfig = array('wubatongcheng','soufun','ganji');

// 贵州省配置 --------------------------------------------------
// 贵阳市
$guiyangConfig = array(
        'wubatongcheng' => 'http://gy.58.com/zufang',               // 58同城网址
        //'wubatongchengSite' => 'http://gy.58.com/zufang/8949044171139x.shtml',               // 58同城网址
        'name' => GUIZHSHENG.GUIYANGSHI                                 // 专区名字
        );

// 铜仁市
$tongrenConfig = array(
        'wubatongcheng' => 'http://tr.58.com/zufang',              // 58同城网址
        'name' => GUIZHSHENG.TONGRENSHI                                // 专区名字
        );

// 遵义市
$zunyiConfig = array(
        'wubatongcheng' => 'http://zunyi.58.com/zufang',           // 58同城网址
        'name' => GUIZHSHENG.ZUNYISHI                                  // 专区名字
        );

// 安顺市
$anshunConfig = array(
        'wubatongcheng' => 'http://anshun.58.com/zufang',          // 58同城网址
        'name' => GUIZHSHENG.ANSHUNSHI                                 // 专区名字
        );

// 六盘水市
$liupanshuiConfig = array(
        'wubatongcheng' => 'http://lps.58.com/zufang',             // 58同城网址
        'name' => GUIZHSHENG.LIUPANSHUISHI                             // 专区名字
        );

// 毕节市
$bijieConfig = array(
        'wubatongcheng' => 'http://bijie.58.com/zufang',           // 58同城网址
        'name' => GUIZHSHENG.BIJIESHI                                  // 专区名字
        );

?>
