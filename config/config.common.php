<?php
/*
   ------------------------------------------------------------
   公共基础配置
   功能说明: 公共的程序配置
   作者: pinyht
   ------------------------------------------------------------
 */

/*
 * 一般配置
 */

define('DEBUG_MODE',false);       // 是否开启debug模式,开启debug模式不会进行写入数据库操作,用于抓取报错时调试用
define('ROOT_PATH',str_replace('/config','',__DIR__));       // 定义程序根目录
define('SITE_PATH',ROOT_PATH.'/site');                       // 定义网站数据采集脚本目录
date_default_timezone_set('Asia/Shanghai');                  // 设置当前系统时区
ini_set("max_execution_time", 2400);                         // 进程最长执行时长,40分钟
ini_set("memory_limit", "500M");                             // 修改进程允许最大内存
define('TODAY_YMD', date('Y-m-d'));                  // 设置当天日期,不补零,用于判断信息是否为当天发布

/*
 * 日志相关配置
 */

define('LOG_PATH',ROOT_PATH.'/log');        // 定义日志目录
define('LOG_ERROR_FILE','error.log');       // 错误日志文件名
define('LOG_NORMAL_FILE','normal.log');     // 正常日志文件名
define('LOG_WARNING_FILE','warning.log');   // 警告日志文件名
define('LOG_TIME_FORMAT','Y-m-d H:i:s');    // 定义日志日期时间格式
define('LOG_FILE_SIZE','50M');              // 定义日志文件最大容量,如果超出则创建新日志文件
define('LOG_BAK_TIME_FORMAT','YmdHis');     // 定义日志备份文件日期时间格式

/*
 * 数据采集相关配置
 */

define('FETCH_AUTO_JUMP',true);             // 开启自动跳转,自动跳转到Location返回的url
define('FETCH_CONNECT_TIME_OUT',60);        // 设置一次抓取会话的连接超时时长为60秒
define('FETCH_TIME_OUT',120);               // 设置一次抓取会话超时时长为120秒
define('FETCH_RETURNINFO',true);            // 设置抓取函数范围抓取到的信息,而不是直接浏览器输出

/*
 * 缓存相关配置
 */

define('CACHE_PATH',ROOT_PATH.'/cache');      // 定义缓存目录
define('CACHE_DATE',date('Ymd'));             // 定义缓存文件名日期
define('CACHE_TIME_FORMAT','YmdHis');    // 定义日志日期时间格式

 ?>
