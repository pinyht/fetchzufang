<?php
/*
   ------------------------------------------------------------
   公共函数库
   作者: pinyht
   ------------------------------------------------------------
 */

/*
 * 数据抓取函数,返回结果为一个数组,下标0的值为包含header信息数组,下标1为html信息的字符串
 * 参数说明: 
 *      $url -- 需要抓取的url地址
 *      $option -- 附加的参数,以数组形式存储
 */
function Fetch($url,$option=null){
    $ch = curl_init();        // 初始化连接资源
    curl_setopt($ch,CURLOPT_URL,$url);              // 设置url
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,FETCH_AUTO_JUMP);   // 自动302跳转
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,FETCH_CONNECT_TIME_OUT);    // 设置连接超时时间
    curl_setopt($ch,CURLOPT_TIMEOUT,FETCH_TIME_OUT);                   // 设置会话超时时间
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,FETCH_RETURNINFO);          // 设置返回数据
    if(count($option) != 0){
        curl_setopt_array($option);     // 如果有额外参数则设置
        unset($option);
    }
    $reHTML = curl_exec($ch);           // 获取网页数据
    $reHeader = curl_getinfo($ch);      // 获取返回的头信息
    $reArr = array($reHeader,$reHTML);  // 组织返回数据
    unset($reHeader,$reHTML);
    curl_close($ch);                    // 关闭会话,释放资源
    
    return $reArr;
}

/*
 * 写日志函数
 * 参数说明: 
 *      $logType -- 需要写入的日志类型
 *          error -- 错误
 *          normal -- 正常
 *          warning -- 警告
 *      $logInfo -- 需要写入的日志信息
 */
function WriteLog($logType,$logInfo){
    $logErrorFile = LOG_PATH.'/'.LOG_ERROR_FILE;         // 错误日志文件名
    $logNomalFile = LOG_PATH.'/'.LOG_NORMAL_FILE;        // 正常日志文件名
    $logWarningFile = LOG_PATH.'/'.LOG_WARNING_FILE;     // 警告日志文件名
    $logFile = null;
    $timeNow = null;                                     // 记录当前时间
    $timeFormat = LOG_TIME_FORMAT;                       // 日志日期时间格式

    switch($logType){
        case 'error':
            $logFile = $logErrorFile;
            break;
        case 'normal':
            $logFile = $logNomalFile;
            break;
        case 'warning':
            $logFile = $logWarningFile;
            break;
    }
    unset($logErrorFile, $logNomalFile,$logWarningFile);
    // 检查日志文件大小,如果日志不存在会返回false.然后创建日志
    if(CheckLogFile($logFile) == false){
        touch($logFile);  // 新建日志文件
        chmod($logFile,0777);    // 改变权限为777
    }

    $file = fopen($logFile,'a');        // 以写入方式打开日志文件
    unset($logFile);
    $timeNow = date($timeFormat);       // 获取当前时间
    unset($timeFormat);
    fwrite($file,'['.$timeNow.'] ['.$logType.'] '.$logInfo."\n");    // 写入日志
    fclose($file);      // 关闭文件资源
    unset($timeNow, $logType, $logInfo);
}

/*
 * 检查日志文件大小,超出配置容量则重新创建日志文件
 * 参数说明: 
 *      $logType -- 需要写入的日志类型
 *          error -- 错误
 *          normal -- 正常
 *          warning -- 警告
 *      $logInfo -- 需要写入的日志信息
 */
function CheckLogFile($logFile){
    $definedFileSize = null;     // 系统定义的日志文件最大容量
    // 判断文件是否存在
    if(!file_exists($logFile)){
        return false;
    }
    $logFileSize = filesize($logFile);      // 获取文件大小,以字节为单位
    // 通过正则表达式获取配置的最大容量数
    preg_match('/[0-9]*/',LOG_FILE_SIZE,$definedFileSize);      
    // 定义单位为kb的情况
    if(stripos(LOG_FILE_SIZE,'k') !== false){
        $definedFileSize = $definedFileSize[0] * 1024;      // 转换为字节
    }
    // 定义单位为mb的情况
    else if(stripos(LOG_FILE_SIZE,'m') !== false){
        $definedFileSize = $definedFileSize[0] * 1024 * 1024;      // 转换为字节
    }
    // 定义单位为gb的情况
    else if(stripos(LOG_FILE_SIZE,'g') !== false){
        $definedFileSize = $definedFileSize[0] * 1024 * 1024 * 1024;      // 转换为字节
    }
    else{
        $definedFileSize = $definedFileSize[0];
    }
    // 判断日志大小,超过容量则备份清空
    if($logFileSize >= $definedFileSize){
        // 定义备份文件的名字为日志名后接当前时间
        $bakFile = $logFile.'.'.date(LOG_BAK_TIME_FORMAT);      
        rename($logFile,$bakFile);
    }
    unset($logFile);
    unset($definedFileSize);
    unset($logFileSize);
    unset($definedFileSize);
    unset($bakFile);
}

/*
 * 取出url的后缀,用作缓存对比的key
 * 参数说明: 
 *      $url -- url地址,以文件名为结尾的形式
 */

function GetUrlKey($url){
    $urlArr = explode('/',$url);
    unset($url);
    return $urlArr[count($urlArr) - 1];
}

/*
 * 写缓存文件,用于记录当天抓取过的url key以及对应的文章id,用于检查信息重复,
 * 如果当天的缓存文件没有就自动创建
 * 缓存文件名为: 日期_网站名.log        缓存内容格式为: 时间:urlKey:id
 * 参数说明: 
 *      $siteName -- 抓取的站点名,用作缓存文件的前缀
 *      $urlKey -- 需要写入的url key,值为详细页面的url最后的部分
 *      $id -- 需要写入的url key对应的租房信息id
 */
function WriteUrlKeyCache($siteName,$urlKey,$id){
    $cacheFileName = CACHE_PATH.'/'.WUBATONGCHENG_KEY.'_'.CACHE_DATE;    // 定义缓存完整路径
    $timeFormat = CACHE_TIME_FORMAT;        // 获取缓存时间格式
    // 判断文件是否存在
    if(!file_exists($cacheFileName)){
        touch($cacheFileName);      // 新建当日的缓存文件
        chmod($cacheFileName,0777);     // 改变权限为777
    }
    $file = fopen($cacheFileName,'a');        // 以写入方式打开缓存文件
    unset($cacheFileName);
    $timeNow = date($timeFormat);       // 获取当前时间
    unset($timeFormat);
    fwrite($file,$siteName.':'.$timeNow.':'.$urlKey.':'.$id."\n");    // 写入缓存
    fclose($file);      // 关闭文件资源
    unset($siteName,$urlKey,$id);
}

?>
