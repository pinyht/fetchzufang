<?php
/*
   ------------------------------------------------------------
   数据抓取入口文件
   功能说明: 执行抓取逻辑
   作者: pinyht
   ------------------------------------------------------------
 */

// // 测试性能
// xhprof_enable();

// 引入文件部分顺序不能颠倒,有互相的内容调用
require_once('./config/config.lang.php');                       // 引入语言配置
require_once('./config/config.common.php');                     // 引入基础配置
require_once('./config/config.mysql.php');                       // 引入站点配置
require_once('./config/config.site.php');                       // 引入站点配置
require_once('./common/function.php');                          // 引入公共函数库
require_once('./common/simple_html_dom.php');                   // 引入html解析库
require_once('./site/wubatongcheng/wubatongcheng_list.php');    // 引入58同城列表采集文件
require_once('./site/wubatongcheng/wubatongcheng_content.php'); // 引入58同城内容采集文件
require_once('./site/soufun/soufun_list.php');                  // 引入搜房网列表采集文件
require_once('./site/soufun/soufun_content.php');               // 引入搜房网内容采集文件

// 开始进行数据采集
// 58同城
FetchWubatongcheng($guiyangConfig['name'],$guiyangConfig['wubatongcheng']);         // 贵阳
FetchWubatongcheng($tongrenConfig['name'],$tongrenConfig['wubatongcheng']);         // 铜仁
FetchWubatongcheng($zunyiConfig['name'],$zunyiConfig['wubatongcheng']);             // 遵义
FetchWubatongcheng($anshunConfig['name'],$anshunConfig['wubatongcheng']);           // 安顺
FetchWubatongcheng($liupanshuiConfig['name'],$liupanshuiConfig['wubatongcheng']);   // 六盘水
FetchWubatongcheng($bijieConfig['name'],$bijieConfig['wubatongcheng']);             // 毕节

// //测试性能
// $xhprof_data = xhprof_disable();
// require_once './xhprof_lib/utils/xhprof_lib.php';
// require_once './xhprof_lib/utils/xhprof_runs.php';

// $xhprof_runs = new XHProfRuns_Default();
// $run_id = $xhprof_runs->save_run($xhprof_data, 'fetch');
// echo "$run_id&fetch";

?>
