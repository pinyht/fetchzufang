<?php
/*
   ------------------------------------------------------------
   58同城网站详细页面数据采集脚本
   功能说明: 执行数据采集
   作者: pinyht
   ------------------------------------------------------------
 */

// $infoUrl -- 详细页面的url地址
// $siteName -- 站点的省市名
function FetchWubatongchengInfo($siteName,$infoUrl){
    // 返回的信息数组,键名和数据库表的字段名一致
    $hsid = null;                     // 租房信息id
    $uid = WUBATONGCHENG_ID;          // 用户id -- wubatongcheng
    $hsuser = null;                   // 联系人名字
    $usertype = null;                 // 联系人类别 -- 0:个人 1:中介
    $tel = null;                      // 联系人电话
    $title = null;                    // 租房信息标题
    $inputtime = date ( 'YmdHis' );   // 发布时间,取当前脚本运行时间
    $inputtime = date ( 'YmdHis' );   // 更新时间,取当前脚本运行时间
    $rent = null;                     // 租金
    $hstype = null;                   // 户型-
    $area = null;                     // 面积-
    $leases = null;                   // 租赁方式
    $address = null;                  // 地址-
    $floor = null;                    // 楼层-
    $rid = array();                   // 所属区域id,数组形式保存,从父级到子级-
    $pay = null;                      // 支付方式
    $xiaoqu = null;                   // 小区-
    $chaoxiang = null;                // 朝向-
    $tenement = null;                 // 物业,房屋类型,住宅或写字楼之类的-
    $spruce = null;                   // 装修-
    $intime = null;                   // 入住时间-
    $fitment = null;                  // 房屋配套-
    $traffic = null;                  // 交通
    $hsinfo = null;                   // 介绍-
    $hspic = array();                 // 房屋图片-
    $fid = null;                      // 区域父级ID
    $name = null;                     // 区域名
    $chid = null;                     // 缓存id
    $url = null;                      // urlKey
    
    $logInfo = WUBATONGCHENG.' '.$siteName.' ';     // log信息
    // 开始抓取网页详细信息
    list($fetchHeader,$fetchHTML) = Fetch($infoUrl);
    if($fetchHeader['http_code'] != 200 && $fetchHeader['http_code'] != 304){
        sleep(5);    // 抓取不到的情况暂停5秒尝试重新抓取
        Fetch($infoUrl);
        if($fetchHeader['http_code'] != 200 && $fetchHeader['http_code'] != 304){
            // 如果状态码不等于200或者304表示没有抓取到信息网页
            WriteLog('error',$logInfo.XIANGXIYEMIAN.' '.$infoUrl.' '.FETCH_FAILD);
            return false;        //此处发邮件
        }
    }
    $htmlDom = str_get_html($fetchHTML);        // 创建网页节点
    $titleInfo = $htmlDom -> find('div.headline',0);
    // 判断信息是否为当天发布
    $isToday = $titleInfo->find('div.other',0);
    // 屏蔽掉长期置顶的垃圾中介信息,这类信息是用js代码展示发布日期,常年为当天的日期
    if(strpos($isToday, TODAY_YMD) === false){
        return false;
    }
    // 获取租房信息标题
    $title = $titleInfo->find('h1');
    if($title){
        $title = $title[0]->find('text',0);
    }
    if(!$title){
        // 获取不到租房信息标题
        WriteLog('warning',$logInfo.ZUFANGXINXIBIAOTI.' '.$infoUrl.' '.GET_FAILD);
    }

    // 获取联系人名字
    preg_match('/username:\'.*\'/', $fetchHTML, $hsuser);
    if($hsuser){
        $hsuser = str_replace('username:','',$hsuser[0]);
        $hsuser = str_replace('\'','',$hsuser);
    }
    else{
        // 获取不到联系人名字
        WriteLog('warning',$logInfo.LIANXIRENMINGZI.' '.$infoUrl.' '.GET_FAILD);
    }
    // 获取联系人类别
    preg_match('/agencyname:\'.*\'/', $fetchHTML, $usertype);
    if($usertype){
        $usertype = str_replace('agencyname:','',$usertype[0]);
        $usertype = str_replace('\'','',$usertype);
    }
    else{
        // 获取不到联系人类别
        WriteLog('warning',$logInfo.LIANXIRENLEIBIE.' '.$infoUrl.' '.GET_FAILD);
    }
    // 获取房屋图片
    preg_match_all('/img_list\.push\(\".*?\"/is', $fetchHTML, $hspicImgs);
    if($hspicImgs){
        foreach($hspicImgs[0] as $hspicImg){
            $hspicImg = str_replace('img_list.push','',$hspicImg);
            $hspicImg = str_replace('(','',$hspicImg);
            $hspicImg = str_replace(')','',$hspicImg);
            $hspicImg = str_replace('"','',$hspicImg);
            $hspicImg = str_replace('/tiny/','/big/',$hspicImg);
            array_push($hspic, $hspicImg);
        }
    }
    if(!$hspic){
        // 获取不到图片
        WriteLog('warning',$logInfo.TUPIAN.' '.$infoUrl.' '.GET_FAILD);
    }
    unset($fetchHeader, $fetchHTML);
    // 获取联系人电话
    $tel = $htmlDom -> find('span[id=t_phone]',0);
    if($tel){
        $telText = $tel->find('text',0);
        // 没有找到电话号码文本,尝试查找图片
        if($telText){
            $tel = $telText;
        }
        else{
            $telImg = $tel->find('img',0)->src;     // 取图片电话号码的地址
            $tel = $telImg;
        }
    }
    // 没有获取到电话号码或者信息过期就不抓取
    if(!$tel || preg_match('/\w+/', $tel) == 0){
        // 获取不到联系人电话
        WriteLog('warning',$logInfo.LIANXIRENDIANHUA.' '.$infoUrl.' '.GET_FAILD);
        return false;    // 获取不到电话等于无用信息
    }
    // 获取租金
    $rent = $htmlDom -> find('span.pri');
    if($rent){
        $rentMoney = $rent[0]->find('text',0);
        if($rentMoney){
            $rent = $rentMoney;    // 获取到金额
        }
        else{
            // 尝试获取租金是否为"面议"
            $rentMianyi = $htmlDom->find('span.mianyi',0);
            $rentMianyi = $rentMianyi->find('text',0);
            $rent = $rentMianyi;
        }
    }
    if(!$rent){
        // 获取不到租金
        WriteLog('warning',$logInfo.ZUJIN.' '.$infoUrl.' '.GET_FAILD);
    }
    // 获取房屋户型,面积,物业类型等
    $hsInfo = $htmlDom -> find('div.infotxt li');
    foreach($hsInfo as $hsInfoLi){
        if(strpos($hsInfoLi, '入　住') !== false){
            $hsInfoText = $hsInfoLi ->find('text', 1);
            $hsInfoText = explode("　", $hsInfoText);
            $intime= $hsInfoText[0];       // 获取入住时间
        }
        if(strpos($hsInfoLi, '户　型') !== false){
            $hsInfoText = $hsInfoLi ->find('text', 1);
            $hsInfoText = explode("　", $hsInfoText);
            $hstype = $hsInfoText[0];       // 获取户型
            $area = $hsInfoText[1];         // 获取面积
        }
        if(strpos($hsInfoLi, '类　型') !== false){
            $hsInfoText = $hsInfoLi ->find('text', 1);
            $hsInfoText = explode("　", $hsInfoText);
            $tenement = $hsInfoText[0];     // 获取物业类型
            for($i = 1; $i < count($hsInfoText); $i++){
                if(strpos($hsInfoText[$i], '装修') !== false){
                    $spruce = $hsInfoText[$i];       // 获取装修
                }
                if(strpos($hsInfoText[$i],'朝向') !== false){
                    $chaoxiang = $hsInfoText[$i];       // 获取朝向
                }
                if(strpos($hsInfoText[$i], '层') !== false){
                    $floor .= ($floor == null ? '' : ' ').$hsInfoText[$i];       // 获取楼层
                }
            }
        }
        if(strpos($hsInfoLi, '配　置') !== false){
            // 获取房屋配套
            $hsInfoText = $hsInfoLi->find('script',0);
            preg_match('/var\stmp\s=\s\'.*\';/',$hsInfoText,$hsInfoText);
            $hsInfoText = str_replace('var tmp = ','',$hsInfoText[0]);
            $hsInfoText = str_replace('\'','',$hsInfoText);
            $hsInfoText = str_replace(';','',$hsInfoText);
            $fitment = $hsInfoText;         // 获取房屋配套
        }
        if(strpos($hsInfoLi, '区　域') !== false){
            $hsInfoText = $hsInfoLi->find('a');
            foreach($hsInfoText as $hsInfoTextA){
                $hsInfoText = $hsInfoTextA->find('text',0);
                if($hsInfoText){
                    array_push($rid, $hsInfoText);      // 区域id为数组形式,从父级到子级
                }
            }
        }
        if(strpos($hsInfoLi, '小　区') !== false){
            $hsInfoText = $hsInfoLi ->find('text', 1);
            $xiaoqu = $hsInfoText;          // 获取小区名字
        }
        if(strpos($hsInfoLi, '地　址') !== false){
            $hsInfoText = $hsInfoLi ->find('text', 1);
            $address = $hsInfoText;         // 获取地址
        }
    }
    if(!$intime){
        // 获取不到入住时间
        WriteLog('warning', $logInfo.RUZHUSHIJIAN.' '.$intime.' '.$infoUrl.GET_FAILD);
    }
    if(!$hstype){
        // 获取不到房屋类型
        WriteLog('warning', $logInfo.HUXING.' '.$hstype.' '.$infoUrl.GET_FAILD);
    }
    if(!$area){
        // 获取不到面积
        WriteLog('warning', $logInfo.MIANJI.' '.$area.' '.$infoUrl.GET_FAILD);
    }
    if(!$tenement){
        // 获取不到物业类型
        WriteLog('warning', $logInfo.WUYELEIXING.' '.$tenement.' '.$infoUrl.GET_FAILD);
    }
    if(!$spruce){
        // 获取不到装修
        WriteLog('warning', $logInfo.ZHUANGXIU.' '.$spruce.' '.$infoUrl.GET_FAILD);
    }
    if(!$chaoxiang){
        // 获取不到朝向
        WriteLog('warning', $logInfo.CHAOXIANG.' '.$chaoxiang.' '.$infoUrl.GET_FAILD);
    }
    if(!$floor){
        // 获取不到楼层
        WriteLog('warning', $logInfo.LOUCENG.' '.$floor.' '.$infoUrl.GET_FAILD);
    }
    if(!$fitment){
        // 获取不到配套
        WriteLog('warning', $logInfo.PEITAO.' '.$fitment.' '.$infoUrl.GET_FAILD);
    }
    if(!$rid){
        // 获取不到区域
        WriteLog('warning', $logInfo.QUYU.' '.$rid.' '.$infoUrl.GET_FAILD);
    }
    if(!$xiaoqu){
        // 获取不到小区名字
        WriteLog('warning', $logInfo.XIAOQU.' '.$xiaoqu.' '.$infoUrl.GET_FAILD);
    }
    if(!$address){
        // 获取不到地址
        WriteLog('warning', $logInfo.DIZHI.' '.$address.' '.$infoUrl.GET_FAILD);
    }
    // 获取介绍信息,介绍信息里包含很多子标签,循环取出所有标签的文本部分
    $hsinfo = $htmlDom -> find('div.maincon', 0);
    $hsinfo = $hsinfo->find('text');
    $hsinfoText = '';
    foreach($hsinfo as $text){
        $hsinfoText .= $text;
    }
    $hsinfo = $hsinfoText;
    if(!$hsinfo){
        // 获取不到介绍
        WriteLog('warning',$logInfo.JIESHAO.' '.$infoUrl.' '.GET_FAILD);
    }
    
   //echo $infoUrl.' '.$intime.' '.$hstype.' '.$area.' '.$tenement.' '.$spruce.' '.$chaoxiang .' '.$floor.' '.$fitment.' '.$rid.' '.$xiaoqu.' '.$address;exit;
    
    writelog('normal',$logInfo.' '.$hsuser.' '.$usertype.' '.$tel.' '.$title.' '.$rent.' '.$infoUrl.' '.$intime.' '.$hstype.' '.$area.' '.$tenement.' '.$spruce.' '.$chaoxiang .' '.$floor.' '.$fitment.' '.$rid[0].' '.$xiaoqu.' '.$address.' '.$hsinfo.' '.$hspic[0].' '.'获取成功');
}
?>
