<?php
/*
   ------------------------------------------------------------
   58同城网站信息列表数据采集脚本
   功能说明: 执行数据采集
   作者: pinyht
   ------------------------------------------------------------
 */

// $siteName -- 站点的省市名
// $siteUrl -- 信息列表的url
// $pageIndex -- 页码
function FetchWubatongcheng($siteName,$siteUrl,$pageIndex = null){
    $htmlDom = null;        // 网页html节点
    $pagePath = null;       // 翻页部分url
    $logInfo = WUBATONGCHENG.' '.$siteName.' ';     // log信息
    if($pageIndex != null){
        $pagePath = '/pn'.$pageIndex.'/';
    }
    // 开始抓取网页列表信息
    list($fetchHeader,$fetchHTML) = Fetch($siteUrl.$pagePath);
    if($fetchHeader['http_code'] != 200){
        sleep(5);    // 抓取不到的情况暂停5秒尝试重新抓取
        Fetch($siteUrl.$pagePath);
        if($fetchHeader['http_code'] != 200){
            // 如果状态码不等于200表示没有抓取到列表网页
            WriteLog('error',$logInfo.LIEBIAOYEMIAN.' '.$siteUrl.$pagePath.' '.FETCH_FAILD);
            return false;        //此处发邮件
        }
    }
    else{
        //WriteLog('normal',$logInfo.LIEBIAOYEMIAN.' '.$siteUrl.$pagePath.' '.FETCH_SUCCES);
        WriteLog('error',$logInfo.LIEBIAOYEMIAN.' '.$siteUrl.$pagePath.' '.FETCH_SUCCES);
    }
    $htmlDom = str_get_html($fetchHTML);        // 创建网页节点
    unset($fetchHeader, $fetchHTML);
    // 获取信息列表
    $infoListTable = $htmlDom -> find('table',0);
    
    if($infoListTable == null){
        // 抓取不到信息列表
        WriteLog('error',$logInfo.XINXILIEBIAO.' '.$siteUrl.$pagePath.' '.GET_FAILD);
    }
    unset($pagePath);
    unset($logInfo);
    $infoList = $infoListTable -> find('tr');
    // 循环读取租房信息列表,最后一行是广告不用读取,第一行是虚假消息,实际不是当天信息
    for($i = 1; $i < count($infoList) - 1; $i++){
        // 取出每条信息的日期,如果等于头一天的日期,就停止抓取
        $infoDate = $infoList[$i] -> find('td');
        $infoDate = $infoDate[count($infoDate) - 1];
        // 判断当前信息是否为当天发布,当出现日期的格式代表不为当天信息
        //if(preg_match('/[0-9]{2}\-[0-9]{2}/',$infoDate -> find('text',0))){
        if(preg_match('/[0-9]{2}\-05/',$infoDate -> find('text',0))){
            return false;
        }
        else{
            // 取出每条信息的url地址
            $infoUrl = $infoList[$i] -> find('a',0) -> href;
            // 判断当前标题是租房信息还是广告,租房信息是以.shtml结尾的url
            if(stripos($infoUrl, '.shtml') === false){
                continue;
            }
            /* 此处调用详细页面抓取
             * 
             *  
             *  */
            FetchWubatongchengInfo($siteName,$infoUrl);        // 获取详细信息
            $infoUrlKey = GetUrlKey($infoUrl);    // 获取urlKey,写入缓存用
            //WriteUrlKeyCache($siteName,$infoUrlKey,'123123');    // 测试写入缓存

            $infoDate -> clear();
        }
    }
    if($pageIndex != null){
        $pageIndex++;       // 翻页
    }
    else{
        $pageIndex = 2;     // 没有页码参数,代表接下来是抓取第二页
    }
    $htmlDom->clear();
    FetchWubatongcheng($siteName,$siteUrl,$pageIndex);
}

?>
